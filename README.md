# OAuth Resource Server

## Build

To build the commands, juste use the Makefile:

```shell
make
```

The commands can be found in the directory `build`.

## Server API

API needs a MongoDB database. You can quickly run database with docker:

```shell
docker run -p 27017:27017 mongo:latest
```

Then, run the API:

```
./build/api
```

## Populate the database

### Generates JSON data

You can generate random data to populate the database using https://next.json-generator.com/.

> Already generated JSON samples can be found in the folder **sample/**.

#### Generate users

```js
[
  {
    'repeat(20)': {
      username: '{{firstName()}}',
      email: function(tags){
        return `${this.username}@${tags.company()}${tags.domainZone()}`.toLowerCase();
      },
      hash: '$2a$14$TrbCIt7JJ2irx2PC9uTfDOtOt66j8vDqcQ07I8JbbxOoIDDMlqnHa'
    }
  }
]
```

Using this template to generate a JSON, the password will be `password` for each user.

#### Generate todos

```js
[
  {
    'repeat(100)': {
      name: function(tags){
        return tags.lorem(tags.integer(5, 8), "words");
      },
      completed: '{{bool()}}',
      deadline: '{{moment(this.date(new Date(2018, 0, 1), new Date(2020, 0, 1))).format("YYYY-MM-DD[T]HH:mm:ssZ")}}'
    }
  }
]
```

### Populate the database with the generated JSON

To populate the database using the previous generated JSON :

```shell
./build/populate sample/sample-users.json sample/sample-todos.json
```

Each todo will be attributed to a random user.

## Tests

Like the API, the tests require a MongoDB database running.

```shell
docker run -p 27017:27017 mongo:latest
```

To tests all Golang packages.

```shell
make test
```
