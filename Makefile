# @Makefile
# OAuth Resource Server Makefile

## Targets

.PHONY: all help deps build test docker deploy clean

all: deps build

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  <no args>				to run build"
	@echo "  help					to display this help to stdout"
	@echo "  deps					to install all dependencies"
	@echo "  build					to build all commands"
	@echo "  test					to test all packages"
	@echo "  docker					to make docker images"
	@echo "  deploy					to deploy the api using docker-compose"
	@echo "  clean					to delete the previous build folder"

deps:
	# Build dependencies
	go get -v -d ./src/...
	# Dev dependencies
	go get -v -d "github.com/stretchr/testify/assert"

build: clean
	# Make the build folder
	mkdir build
	# Build the commands
	go build -o build/api src/cmd/api/api.go
	go build -o build/populate src/cmd/populate/populate.go

test:
	# Test all packages
	@MONGO_DATABASE=test \
	go test ./src/...

docker: build
	# Make docker images
	docker build -t data-api -f docker/data-api/Dockerfile .

deploy: docker
	# Create and start containers
	docker-compose up -d

clean:
	# Remove the previous build folder
	rm -rf build
