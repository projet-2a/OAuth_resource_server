package main

import (
	"../../config"
	"../../server"
)

func main() {
	server.Init(config.MONGO_SERVER, config.MONGO_DATABASE)
	server.Serve(config.LISTENING_IP, config.LISTENING_PORT)
}
