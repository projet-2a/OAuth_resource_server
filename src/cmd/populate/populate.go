package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"../../config"
	"../../server/dao"
)

func LoadUsersFromJSON(usersFilename string) ([]dao.User, error) {
	users := []dao.User{}
	// Open the JSON file for users
	usersJSON, err := os.Open(usersFilename)
	// Handle os.Open errors
	if err != nil {
		return nil, err
	}
	// Defer the closing of the JSON file
	defer usersJSON.Close()
	// Parse the file
	decoder := json.NewDecoder(usersJSON)
	err = decoder.Decode(&users)
	// Handle decode error
	if err != nil {
		return nil, err
	}
	return users, nil
}

func LoadTodosFromJSON(todosFilename string) ([]dao.Todo, error) {
	todos := []dao.Todo{}
	// Open the JSON file for users
	todosJSON, err := os.Open(todosFilename)
	// Handle os.Open errors
	if err != nil {
		return nil, err
	}
	// Defer the closing of the JSON file
	defer todosJSON.Close()
	// Parse the file
	decoder := json.NewDecoder(todosJSON)
	err = decoder.Decode(&todos)
	// Handle decode error
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func InsertUsersInDB(users []dao.User) error {
	// Initialization
	userDAO := dao.UserDAO{}
	// Insert users
	for _, user := range users {
		err := userDAO.Insert(user)
		if err != nil {
			return err
		}
	}
	// No errors
	return nil
}

func InsertTodosInDB(todos []dao.Todo) error {
	// Initialization
	userDAO := dao.UserDAO{}
	todoDAO := dao.TodoDAO{}
	users, _ := userDAO.FindAll()
	rand.Seed(time.Now().Unix())
	// Insert todos
	for _, todo := range todos {
		// Pick a random UserID
		randomUserID := users[rand.Intn(len(users))].ID
		todo.UserID = randomUserID
		// Insert the todo
		err := todoDAO.Insert(todo)
		if err != nil {
			return err
		}
	}
	// No errors
	return nil
}

func Populate(usersFilename string, todosFilename string) error {
	// Connect to MongoDB and initialize the DAO
	dao.Connect(config.MONGO_SERVER, config.MONGO_DATABASE)
	log.Print("Connected to MongoDB database " + config.MONGO_DATABASE)
	// Load users
	log.Print("Loading users from " + usersFilename)
	users, err := LoadUsersFromJSON(usersFilename)
	if err != nil {
		return err
	}
	// Insert users
	log.Printf("Inserting %d users", len(users))
	err = InsertUsersInDB(users)
	if err != nil {
		return err
	}
	// Load todos
	log.Print("Loading todos from " + todosFilename)
	todos, err := LoadTodosFromJSON(todosFilename)
	if err != nil {
		return err
	}
	// Insert todos
	log.Printf("Inserting %d todos", len(todos))
	err = InsertTodosInDB(todos)
	if err != nil {
		return err
	}
	// If we are here, no error has happened
	return nil
}

func main() {
	args := os.Args

	if len(args) != 3 {
		fmt.Printf("Usage: %s users.json todos.json\n", args[0])
		os.Exit(1)
	}
	err := Populate(args[1], args[2])
	// Handle errors
	if err != nil {
		log.Fatal(err)
	}
}
