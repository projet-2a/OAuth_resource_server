package server

import (
	"log"
	"net/http"

	"./dao"
	"github.com/rs/cors"
)

var userDAO = dao.UserDAO{}
var todoDAO = dao.TodoDAO{}

func Init(mongo_server string, mongo_database string) {
	dao.Connect(mongo_server, mongo_database)
	log.Print("Connected to MongoDB database " + mongo_database)
}

func Serve(server_ip string, server_port string) {
	// Make router
	router := NewRouter()
	handler := cors.AllowAll().Handler(router)
	// Listen and server
	listenOn := server_ip + ":" + server_port
	log.Print("Listening on " + listenOn)
	log.Fatal(http.ListenAndServe(listenOn, handler))
}
