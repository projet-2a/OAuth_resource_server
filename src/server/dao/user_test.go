package dao

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/mgo.v2/bson"
)

func TestUserDAOFindAll(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindAll
	_users, err := userDAO.FindAll()
	// Does FindAll terminate normally ?
	assert.Nil(t, err)
	// Test for the number of users
	assert.Equal(t, len(_users), 3)
	// Test for users fields
	for i := range users {
		assert.Equal(t, users[i], _users[i])
	}
}

func TestUserDAOFindById(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindById
	user, err := userDAO.FindById(users[0].ID)
	// Does Find terminate normally ?
	assert.Nil(t, err)
	// Test for the user fields
	assert.Equal(t, users[0], user)
}

func TestUserDAOFindByUsername(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindByUsername
	user, err := userDAO.FindByUsername(users[0].Username)
	// Does Find terminate normally ?
	assert.Nil(t, err)
	// Test for the user fields
	assert.Equal(t, users[0], user)
}

func TestUserDAOInsert(t *testing.T) {
	// Initialization
	initializeDatabase()

	// Insert a new user
	user := User{
		ID:       bson.NewObjectId(),
		Username: "testUsername",
		Email:    "itsme@mar.io",
	}
	err := userDAO.Insert(user)

	// Test for the insertion
	assert.Nil(t, err)

	_users, _ := userDAO.FindAll()
	assert.Equal(t, len(_users), 4)

	insertedUser, err := userDAO.FindById(user.ID)
	assert.Nil(t, err)
	assert.Equal(t, user, insertedUser)

	// Try to reinsert a user with the same username
	err = userDAO.Insert(user)
	assert.NotNil(t, err)
}

func TestUserDAODelete(t *testing.T) {
	// Initialization
	initializeDatabase()

	// Delete some users
	userDAO.Delete(users[1])
	userDAO.Delete(users[2])

	// Test for the deletion
	_users, _ := userDAO.FindAll()
	assert.Equal(t, len(_users), 1)

	for _, user := range _users {
		assert.NotEqual(t, users[1], user)
		assert.NotEqual(t, users[2], user)
	}
}

func TestUserDAOUpdate(t *testing.T) {
	// Initialization
	initializeDatabase()

	// Update a todo
	user := users[2]
	user.Username = "UpdAt3d"
	user.Email = "update@email.com"
	userDAO.Update(user)

	// Test for the update
	updatedUser, _ := userDAO.FindById(user.ID)
	assert.Equal(t, user, updatedUser)
}
