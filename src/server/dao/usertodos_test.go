package dao

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUserDAOFindByIdTodos(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindByIdTodos
	userTodos, err := userDAO.FindByIdTodos(users[2].ID)
	// Does FindAll terminate normally ?
	assert.Nil(t, err)
	// Test for the user fields
	assert.Equal(t, users[2].ID, userTodos.ID)
	assert.Equal(t, users[2].Username, userTodos.Username)
	assert.Equal(t, users[2].Email, userTodos.Email)
	// Test for the number of todos
	assert.Equal(t, len(userTodos.Todos), 2)
	// Test for todos fields
	assert.True(t, todos[2].Equal(userTodos.Todos[0]), "Todos not equal")
	assert.True(t, todos[3].Equal(userTodos.Todos[1]), "Todos not equal")
}

func TestUserDAOFindByUsernameTodos(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindByUsernameTodos
	userTodos, err := userDAO.FindByUsernameTodos(users[2].Username)
	// Does FindAll terminate normally ?
	assert.Nil(t, err)
	// Test for the user fields
	assert.Equal(t, users[2].ID, userTodos.ID)
	assert.Equal(t, users[2].Username, userTodos.Username)
	assert.Equal(t, users[2].Email, userTodos.Email)
	// Test for the number of todos
	assert.Equal(t, len(userTodos.Todos), 2)
	// Test for todos fields
	assert.True(t, todos[2].Equal(userTodos.Todos[0]), "Todos not equal")
	assert.True(t, todos[3].Equal(userTodos.Todos[1]), "Todos not equal")
}
