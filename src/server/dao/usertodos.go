package dao

import (
	"gopkg.in/mgo.v2/bson"
)

// User model
type UserTodos struct {
	ID       bson.ObjectId `bson:"_id" json:"id"`
	Username string        `bson:"username" json:"username"`
	Email    string        `bson:"email" json:"email"`
	Todos    []Todo        `json:"todos"`
}

// FindByIdTodos find a user and his todos by its id
func (m *UserDAO) FindByIdTodos(id bson.ObjectId) (UserTodos, error) {
	var userTodos UserTodos
	// err := db.C(USERS_COLLECTION).FindId(id).One(&user)
	err := db.C(USERS_COLLECTION).Pipe([]bson.M{{"$match": bson.M{"_id": id}}, {"$lookup": bson.M{"from": "todos", "localField": "_id", "foreignField": "user_id", "as": "todos"}}, {"$limit": 1}}).One(&userTodos)
	return userTodos, err
}

// FindByUsernameTodos find a user and his todos by its username
func (m *UserDAO) FindByUsernameTodos(username string) (UserTodos, error) {
	var userTodos UserTodos
	// err := db.C(USERS_COLLECTION).Find(bson.M{"username": username}).One(&user)
	err := db.C(USERS_COLLECTION).Pipe([]bson.M{{"$match": bson.M{"username": username}}, {"$lookup": bson.M{"from": "todos", "localField": "_id", "foreignField": "user_id", "as": "todos"}}, {"$limit": 1}}).One(&userTodos)
	return userTodos, err
}
