package dao

import (
	"../../config"
	"gopkg.in/mgo.v2/bson"
)

var todoDAO = TodoDAO{}
var userDAO = UserDAO{}

var users []User
var todos []Todo

func initializeDatabase() {
	// Initialize the DAO and connect to MongoDB
	Connect(config.MONGO_SERVER, config.MONGO_DATABASE)
	// Clean the collections
	db.C(USERS_COLLECTION).RemoveAll(nil)
	db.C(TODOS_COLLECTION).RemoveAll(nil)
	// Populate the database with users
	users = []User{
		User{
			ID:       bson.NewObjectId(),
			Username: "mathieu11",
			Email:    "mathieu11@email.com",
		},
		User{
			ID:       bson.NewObjectId(),
			Username: "mathieu12",
			Email:    "mathieu12@email.com",
		},
		User{
			ID:       bson.NewObjectId(),
			Username: "mathieu13",
			Email:    "mathieu13@email.com",
		},
	}
	for _, user := range users {
		userDAO.Insert(user)
	}
	// Populate the database with todos
	// Deadlines
	deadline1, _ := StringToTime("2018-10-30T13:44:59+01:00")
	deadline2, _ := StringToTime("2019-01-30T13:46:59+01:00")
	deadline3, _ := StringToTime("2020-01-30T13:23:02+01:00")
	deadline4, _ := StringToTime("2018-03-30T23:20:02+01:00")
	// Todos
	todos = []Todo{
		Todo{
			ID:        bson.NewObjectId(),
			UserID:    users[0].ID,
			Name:      "Wash the dishes.",
			Completed: false,
			Deadline:  deadline1,
		},
		Todo{
			ID:        bson.NewObjectId(),
			UserID:    users[1].ID,
			Name:      "Eat water.",
			Completed: true,
			Deadline:  deadline2,
		},
		Todo{
			ID:        bson.NewObjectId(),
			UserID:    users[2].ID,
			Name:      "Burn the birds.",
			Completed: false,
			Deadline:  deadline3,
		},
		Todo{
			ID:        bson.NewObjectId(),
			UserID:    users[2].ID,
			Name:      "Do something.",
			Completed: true,
			Deadline:  deadline4,
		},
	}
	for _, todo := range todos {
		todoDAO.Insert(todo)
	}
}
