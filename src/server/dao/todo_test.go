package dao

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/mgo.v2/bson"
)

func TestTodoDAOFindAll(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindAll
	_todos, err := todoDAO.FindAll()
	// Does FindAll terminate normally ?
	assert.Nil(t, err)
	// Test for the number of todos
	assert.Equal(t, len(_todos), 4)
	// Test for todos fields
	for i := range todos {
		assert.True(t, todos[i].Equal(_todos[i]), "Todos not equal")
	}
}

func TestTodoDAOFindByUserId(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindByUserId
	_todos, err := todoDAO.FindByUserId(todos[0].UserID)
	// Does Find terminate normally ?
	assert.Nil(t, err)
	// Test for the number of tdos
	assert.Equal(t, len(_todos), 1)
	// Test for the todo fields
	assert.True(t, todos[0].Equal(_todos[0]), "Todos not equal")
}

func TestTodoDAOFindById(t *testing.T) {
	// Initialization
	initializeDatabase()
	// FindById
	todo, err := todoDAO.FindById(todos[0].ID)
	// Does Find terminate normally ?
	assert.Nil(t, err)
	// Test for the todo fields
	assert.True(t, todos[0].Equal(todo), "Todos not equal")
}

func TestTodoDAOInsert(t *testing.T) {
	// Initialization
	initializeDatabase()

	// Insert a new todo
	deadline, _ := StringToTime("2048-04-20T13:22:12+01:00")
	todo := Todo{
		ID:        bson.NewObjectId(),
		UserID:    users[2].ID,
		Name:      "Test everything!",
		Completed: false,
		Deadline:  deadline,
	}
	todoDAO.Insert(todo)

	// Test for the insertion
	_todos, _ := todoDAO.FindAll()
	assert.Equal(t, len(_todos), 5)

	insertedTodo, err := todoDAO.FindById(todo.ID)
	assert.Nil(t, err)
	assert.True(t, todo.Equal(insertedTodo), "Todos not equal")
}

func TestTodoDAOUpdate(t *testing.T) {
	// Initialization
	initializeDatabase()

	// Update a todo
	todo := todos[3]
	todo.Name = "Updated todo!"
	todo.Completed = false
	todoDAO.Update(todo)

	// Test for the update
	updatedTodo, _ := todoDAO.FindById(todo.ID)
	assert.True(t, todo.Equal(updatedTodo), "Todos not equal")
}

func TestTodoDAODelete(t *testing.T) {
	// Initialization
	initializeDatabase()

	// Delete some todos
	todoDAO.Delete(todos[1])
	todoDAO.Delete(todos[2])

	// Test for the deletion
	_todos, _ := todoDAO.FindAll()
	assert.Equal(t, len(_todos), 2)

	for _, todo := range _todos {
		assert.False(t, todos[1].Equal(todo), "Todos are equal")
		assert.False(t, todos[2].Equal(todo), "Todos are equal")
	}
}

func TestTodoDAODeleteById(t *testing.T) {
	// Initialization
	initializeDatabase()

	// Delete some todos
	todoDAO.DeleteById(todos[1].ID)
	todoDAO.DeleteById(todos[2].ID)

	// Test for the deletion
	_todos, _ := todoDAO.FindAll()
	assert.Equal(t, len(_todos), 2)

	for _, todo := range _todos {
		assert.False(t, todos[1].Equal(todo), "Todos are equal")
		assert.False(t, todos[2].Equal(todo), "Todos are equal")
	}
}
