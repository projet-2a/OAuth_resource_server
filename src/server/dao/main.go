package dao

import (
	"log"
	
	mgo "gopkg.in/mgo.v2"
)

var db *mgo.Database

func Connect(server string, database string) {
	session, err := mgo.Dial(server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(database)
}