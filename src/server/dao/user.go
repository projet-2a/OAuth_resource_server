package dao

import (
	"errors"

	"gopkg.in/mgo.v2/bson"
)

const USERS_COLLECTION = "users"

// User model
type User struct {
	ID       bson.ObjectId `bson:"_id" json:"id"`
	Username string        `bson:"username" json:"username"`
	Email    string        `bson:"email" json:"email"`
}

// UserDAO is the struct used to manipulate users
type UserDAO struct{}

// FindById find a user by its id
func (m *UserDAO) FindById(id bson.ObjectId) (User, error) {
	var user User
	err := db.C(USERS_COLLECTION).FindId(id).One(&user)
	return user, err
}

// FindByUsername find a user by its username
func (m *UserDAO) FindByUsername(username string) (User, error) {
	var user User
	err := db.C(USERS_COLLECTION).Find(bson.M{"username": username}).One(&user)
	return user, err
}

// FindAll the list of all users
func (m *UserDAO) FindAll() ([]User, error) {
	var users []User
	err := db.C(USERS_COLLECTION).Find(bson.M{}).All(&users)
	return users, err
}

// Insert allow to insert a user into database
func (m *UserDAO) Insert(user User) error {
	_, err := m.FindByUsername(user.Username)
	if err != nil {
		if user.ID == "" {
			i := bson.NewObjectId()
			user.ID = i
		}
		err = db.C(USERS_COLLECTION).Insert(&user)
	} else {
		err = errors.New("Username already taken")
	}
	return err
}

// Delete allow to delete an existing user
func (m *UserDAO) Delete(user User) error {
	err := db.C(USERS_COLLECTION).Remove(&user)
	return err
}

// Update allow to update an existing user
func (m *UserDAO) Update(user User) error {
	err := db.C(USERS_COLLECTION).UpdateId(user.ID, &user)
	return err
}
