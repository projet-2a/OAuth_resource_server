package dao

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

const TODOS_COLLECTION = "todos"

// Todo model
type Todo struct {
	ID        bson.ObjectId `bson:"_id" json:"id,omitempty"`
	UserID    bson.ObjectId `bson:"user_id" json:"user_id,omitempty"`
	Name      string        `bson:"name" json:"name,omitempty"`
	Completed bool          `bson:"completed" json:"completed"`
	Deadline  time.Time     `bson:"deadline" json:"deadline,omitempty"`
}

func (todo1 *Todo) Equal(todo2 Todo) bool {
	return (todo1.ID == todo2.ID) && (todo1.UserID == todo2.UserID) &&
		(todo1.Name == todo2.Name) && (todo1.Completed == todo2.Completed) &&
		(todo1.Deadline.Equal(todo2.Deadline))
}

// TodoDAO is the struct used to manipulate todos
type TodoDAO struct{}

// FindAll find all todos
func (m *TodoDAO) FindAll() ([]Todo, error) {
	var todos []Todo
	err := db.C(TODOS_COLLECTION).Find(bson.M{}).All(&todos)
	return todos, err
}

// FindByUserId find all todos for given user
func (m *TodoDAO) FindByUserId(userID bson.ObjectId) ([]Todo, error) {
	var todos []Todo
	err := db.C(TODOS_COLLECTION).Find(bson.M{"user_id": userID}).All(&todos)
	return todos, err
}

// FindById find a todo by it's id
func (m *TodoDAO) FindById(id bson.ObjectId) (Todo, error) {
	var todo Todo
	err := db.C(TODOS_COLLECTION).FindId(id).One(&todo)
	return todo, err
}

// Insert allow to insert a todo
func (m *TodoDAO) Insert(todo Todo) error {
	if todo.ID == "" {
		i := bson.NewObjectId()
		todo.ID = i
	}
	err := db.C(TODOS_COLLECTION).Insert(&todo)
	return err
}

// Update allow to update an existing todo
func (m *TodoDAO) Update(todo Todo) error {
	err := db.C(TODOS_COLLECTION).UpdateId(todo.ID, &todo)
	return err
}

// Delete allow to delete an existing todo
func (m *TodoDAO) Delete(todo Todo) error {
	err := db.C(TODOS_COLLECTION).Remove(&todo)
	return err
}

// DeleteById allow to delete an existing todo by ID
func (m *TodoDAO) DeleteById(id bson.ObjectId) error {
	err := db.C(TODOS_COLLECTION).Remove(bson.M{"_id": id})
	return err
}
