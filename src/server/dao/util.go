package dao

import (
	"time"
)

// StringToTime convert a string to time.Time object
func StringToTime(timeStr string) (time.Time, error) {
	layout := "2006-01-02T15:04:05Z07:00"
	t, err := time.Parse(layout, timeStr)
	return t, err
}
