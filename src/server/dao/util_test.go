package dao

import (
	"log"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestStringToTime(t *testing.T) {
	_time, err := StringToTime("2018-10-30T13:44:59+01:00")
	log.Print(_time)
	// Test for err
	assert.Nil(t, err)
	// Tests for time
	assert.Equal(t, 2018, _time.Year())
	assert.Equal(t, time.Month(10), _time.Month())
	assert.Equal(t, 30, _time.Day())
	assert.Equal(t, 13, _time.Hour())
	assert.Equal(t, 44, _time.Minute())
	assert.Equal(t, 59, _time.Second())
}
