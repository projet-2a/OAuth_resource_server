package server

import (
	"net/http"

	"./dao"
	"github.com/gorilla/mux"
)

func GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := userDAO.FindAll()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "No users found")
		return
	}
	respondWithJson(w, http.StatusOK, users)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	user, err := userDAO.FindByUsername(params["username"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "User not found")
		return
	}
	respondWithJson(w, http.StatusOK, user)
}

func GetUserEmail(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	user, err := userDAO.FindByUsername(params["username"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "User not found")
		return
	}
	respondWithJson(w, http.StatusOK, map[string]string{"email": user.Email})
}

func GetUserTodos(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userTodos, err := userDAO.FindByUsernameTodos(params["username"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "User not found")
		return
	}
	respondWithJson(w, http.StatusOK, userTodos.Todos)
}

func NewUserTodo(w http.ResponseWriter, r *http.Request) {
	// Parse URI params
	params := mux.Vars(r)
	// User ID
	user, err := userDAO.FindByUsername(params["username"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "User not found")
		return
	}
	// New todo
	var newTodo dao.Todo
	newTodo.UserID = user.ID
	// Parse form
	r.ParseForm()
	err = ParseTodoForm(&newTodo, r.Form)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	// Insert the new todo
	err = todoDAO.Insert(newTodo)
	if err == nil {
		respondWithJson(w, http.StatusOK, map[string]string{"status": "Todo created"})
	} else {
		respondWithError(w, http.StatusBadRequest, "Error while creating the todo")
	}
}

func GetUserTodo(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userTodos, err := userDAO.FindByUsernameTodos(params["username"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "User not found")
		return
	}
	todoID, err := StringToObjectId(params["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Todo ID")
		return
	}
	for _, todo := range userTodos.Todos {
		if todo.ID == todoID {
			respondWithJson(w, http.StatusOK, todo)
			return
		}
	}
	respondWithError(w, http.StatusBadRequest, "Todo not found")
}

func UpdateUserTodo(w http.ResponseWriter, r *http.Request) {
	// Parse URI params
	params := mux.Vars(r)
	// User ID
	userTodos, err := userDAO.FindByUsernameTodos(params["username"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "User not found")
		return
	}
	// Todo ID
	todoID, err := StringToObjectId(params["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Todo ID")
		return
	}
	// Find the old todo
	var oldTodo dao.Todo
	for _, todo := range userTodos.Todos {
		if todo.ID == todoID {
			oldTodo = todo
			break
		}
	}
	if oldTodo == (dao.Todo{}) {
		respondWithError(w, http.StatusBadRequest, "Todo not found")
		return
	}
	// Parse form
	r.ParseForm()
	err = ParseTodoForm(&oldTodo, r.Form)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	// Update todo
	err = todoDAO.Update(oldTodo)
	if err == nil {
		respondWithJson(w, http.StatusOK, map[string]string{"status": "Todo updated"})
	} else {
		respondWithError(w, http.StatusBadRequest, "Error while updating the todo")
	}
}

func DeleteUserTodo(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userTodos, err := userDAO.FindByUsernameTodos(params["username"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "User not found")
		return
	}
	todoID, err := StringToObjectId(params["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Todo ID")
		return
	}
	for _, todo := range userTodos.Todos {
		if todo.ID == todoID {
			err = todoDAO.Delete(todo)
			if err == nil {
				respondWithJson(w, http.StatusOK, map[string]string{"status": "Todo deleted"})
			} else {
				respondWithError(w, http.StatusBadRequest, "Error while deleting the todo")
			}
			return
		}
	}
	respondWithError(w, http.StatusBadRequest, "Todo not found")
}
