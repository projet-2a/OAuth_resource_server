package server

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strings"

	"../config"
)

// Introspect object represent the result of the endpoint Introspection
// For extra documentation of JWT fields see: RFC-7519 https://tools.ietf.org/html/rfc7519
type Token struct {

	// Boolean indicator of whether or not the presented token is currently active (Required)
	Active bool `json:"active"`

	// A string containing a space-separated list of scopes associated with this token (Optional)
	Scope string `json:"scope,omitempty"`

	// Client identifier for the OAuth 2.0 client that requested this token (Optional)
	ClientID string `json:"client_id,omitempty"`

	// Human-readable identifier for the resource owner who authorized this token (Optional)
	Username string `json:"username,omitempty"`

	// Type of the OAuth2.0 token (Optional)
	TokenType string `json:"token_type,omitempty"`

	// Integer timestamp indicating when this token will expire (Optional)
	Exp int32 `json:"exp,omitempty"`

	// Integer timestamp indicating when this token was originally issued (Optional)
	Iat int32 `json:"iat,omitempty"`

	// Integer timestamp indicating when this token is not to be used before (Optional)
	Nbf int32 `json:"nbf,omitempty"`

	// Subject of the token, usually a machine-readable identifier of the resource owner who authorized this token. (Optional)
	Sub string `json:"sub,omitempty"`

	// Service-specific string identifier representing the intended audience for this token (Optional)
	Aud string `json:"aud,omitempty"`

	// String representing the issuer of this token (Optional)
	Iss string `json:"iss,omitempty"`

	// String identifier for the token (Optional)
	Jti string `json:"jti,omitempty"`
}

func TokenIntrospect(rawToken string) (Token, error) {
	var token Token
	introspectURL := config.OAUTH_SERVER + "/introspect"
	// HTTP Client
	client := http.Client{}
	// POST params
	data := url.Values{}
	data.Set("token", rawToken)
	data.Set("token_type_hint", "access_token")
	// Make the request object
	req, _ := http.NewRequest("POST", introspectURL, strings.NewReader(data.Encode()))
	// Add headers
	basicAuth := BasicAuth(config.OAUTH_CLIENT_ID, config.OAUTH_CLIENT_SECRET)
	req.Header.Add("Authorization", "Basic "+basicAuth)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	// Make introspect request
	resp, err := client.Do(req)
	if err != nil {
		return token, err
	}
	err = json.NewDecoder(resp.Body).Decode(&token)
	if err != nil {
		return token, err
	}
	return token, nil
}
