package server

import (
	"errors"
	"net/http"
	"strings"
)

func ExtractToken(header string) (string, error) {
	headerParts := strings.Split(header, " ")
	if len(headerParts) != 2 || headerParts[0] != "Bearer" || len(headerParts[1]) == 0 {
		return "", errors.New(
			"Bad authorization header, should be: \"Authorization: Bearer <token>\"")
	}
	return headerParts[1], nil
}

func Authentication(inner http.Handler, scope string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// No scope = no authorization needed
		if scope == "" {
			inner.ServeHTTP(w, r)
			return
		}
		// Extract token from header
		authorizationHeader := r.Header.Get("Authorization")
		if authorizationHeader == "" {
			respondWithError(w, http.StatusUnauthorized, "Authorization header missing")
			return
		}
		rawToken, err := ExtractToken(authorizationHeader)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}
		// Verify token
		token, err := TokenIntrospect(rawToken)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err.Error())
			return
		}
		// Is token active ?
		if !token.Active {
			respondWithError(w, http.StatusUnauthorized, "Bad or inactive token")
			return
		}
		// The case where there is a scope
		for _, _scope := range strings.Split(token.Scope, " ") {
			if _scope == scope {
				inner.ServeHTTP(w, r)
				return
			}
		}
		respondWithError(w, http.StatusForbidden, "Forbidden scope")
	})
}
