package server

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"

	"./dao"
	"github.com/microcosm-cc/bluemonday"
	"gopkg.in/mgo.v2/bson"
)

var htmlPolicy = bluemonday.StrictPolicy()

// Utils

func StringToObjectId(strObjectId string) (bson.ObjectId, error) {
	d, err := hex.DecodeString(strObjectId)
	if err != nil || len(d) != 12 {
		return "", errors.New("Not a valid ObjectId")
	} else {
		return bson.ObjectIdHex(strObjectId), nil
	}
}

func SanitizeHTML(input string) string {
	return htmlPolicy.Sanitize(input)
}

func ParseTodoForm(todo *dao.Todo, form url.Values) error {
	if len(form["name"]) > 0 {
		todo.Name = SanitizeHTML(form["name"][0])
	}
	if len(form["completed"]) > 0 {
		completed, err := strconv.ParseBool(form["completed"][0])
		if err != nil {
			return errors.New("Completed should be 'true' or 'false'")
		}
		todo.Completed = completed
	}
	if len(form["deadline"]) > 0 {
		deadline, err := dao.StringToTime(form["deadline"][0])
		if err != nil {
			return err
		}
		todo.Deadline = deadline
	}
	return nil
}

// HTTP Methods

func NotFound(w http.ResponseWriter, r *http.Request) {
	respondWithError(w, http.StatusNotFound, "Resource not found")
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func BasicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}
