package server

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
	Scope       string
}

type Routes []Route

var routes = Routes{
	Route{
		"GetUsers",
		"GET",
		"/users",
		GetUsers,
		"",
	},
	Route{
		"GetUser",
		"GET",
		"/users/{username}",
		GetUser,
		"",
	},
	Route{
		"GetUserEmail",
		"GET",
		"/users/{username}/email",
		GetUserEmail,
		"email",
	},
	Route{
		"GetUserTodos",
		"GET",
		"/users/{username}/todos",
		GetUserTodos,
		"list",
	},
	Route{
		"NewUserTodo",
		"PUT",
		"/users/{username}/todos",
		NewUserTodo,
		"create",
	},
	Route{
		"GetUserTodo",
		"GET",
		"/users/{username}/todos/{id}",
		GetUserTodo,
		"read",
	},
	Route{
		"UpdateUserTodo",
		"PATCH",
		"/users/{username}/todos/{id}",
		UpdateUserTodo,
		"update",
	},
	Route{
		"DeleteUserTodo",
		"DELETE",
		"/users/{username}/todos/{id}",
		DeleteUserTodo,
		"delete",
	},
}
