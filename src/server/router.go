package server

import (
	"net/http"

	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {
	// Make an empty router
	router := mux.NewRouter().StrictSlash(true)
	// Routes
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)
		handler = Authentication(handler, route.Scope)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)

	}
	// Handle 404
	notFoundHandler := Logger(http.HandlerFunc(NotFound), "NotFound")
	router.NotFoundHandler = notFoundHandler
	// Return the router
	return router
}
