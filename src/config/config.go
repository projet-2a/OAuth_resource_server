package config

import "os"

// Default configuration
var (
	MONGO_SERVER        = "localhost"
	MONGO_DATABASE      = "todo_db"
	LISTENING_IP        = "0.0.0.0"
	LISTENING_PORT      = "4000"
	OAUTH_SERVER        = "https://api.cyberoauth.tk/v1"
	OAUTH_CLIENT_ID     = "e605fe67e4eaec030425"
	OAUTH_CLIENT_SECRET = "719f764a5724843e8a8467c282d56214"
)

// init make the configuration
func init() {
	// Get environment variables
	envMongoServer := os.Getenv("MONGO_SERVER")
	envMongoDatabase := os.Getenv("MONGO_DATABASE")
	envListeningIP := os.Getenv("LISTENING_IP")
	envListeningPort := os.Getenv("LISTENING_PORT")
	// If an environment variable is not empty, take it
	if envMongoServer != "" {
		MONGO_SERVER = envMongoServer
	}
	if envMongoDatabase != "" {
		MONGO_DATABASE = envMongoDatabase
	}
	if envListeningIP != "" {
		LISTENING_IP = envListeningIP
	}
	if envListeningPort != "" {
		LISTENING_PORT = envListeningPort
	}
}
