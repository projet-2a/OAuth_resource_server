package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	assert.NotEqual(t, MONGO_SERVER, "", "MONGO_SERVER constant is empty.")
	assert.NotEqual(t, MONGO_DATABASE, "", "MONGO_DATABASE constant is empty.")
	assert.NotEqual(t, LISTENING_IP, "", "LISTENING_IP constant is empty.")
	assert.NotEqual(t, LISTENING_PORT, "", "LISTENING_PORT constant is empty.")
}
